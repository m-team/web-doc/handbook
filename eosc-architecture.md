[![eosc-future-logo](arch/eosc-future.svg){: style="width: 400px"}](https://eoscfuture.eu)

This section was kindly contributed by the [EOSC-Future project](https://eoscfuture.eu).

# Architecture of EOSC

EOSC develops a platform to support the delivery of resources supporting
research process. It serves two main EOSC stakeholders: Consumers and Providers.
Their interplay and their relation to the EOSC Platform is shown in
Diagram 1.

![eosc-stakeholeders](arch/eosc-stakeholders.svg){: style="width:80%"}

*Diagram 1. High level view on EOSC Platform*

The EOSC Platform is meant to facilitate adding new resources and make
them visible in its Catalogue on one end, and to allow the resource
discovery and utilisation on the other end.
In the broad scope of resources offered, and due to the differences in terms of
delivery and the technological nature, the EOSC Platform implements the
topology presented in diagram 2.

![eosc-topology](arch/topology.png){: style="width:70%"}

*Diagram 2. EOSC Resource topology*

The Platform is ready to support EOSC goals as a result of operation,
connections and integrations between its components. The consumer-facing
layer is called EOSC Exchange and includes EOSC resources supporting
Research and Open Science which are made discoverable and accessible via a
dedicated interface, the EOSC Marketplace. In EOSC Exchange there is a
dedicated category of services called 'Horizontal Services' which can
serve general types of EOSC Consumers and may be used by EOSC Providers to
enrich their services with new capabilities. Services served by computing
centres and e-Infrastructures are recognised as Horizontal services.

Services and research products can be enhanced with means of
professional maintenance and delivery thanks to the EOSC-Core. Both
layers are tightly connected via the Interoperability Framework, that allows a
unified approach towards the integration with EOSC-Core and aims to deliver
guidance towards the (general) composability of resources.

All elements composing EOSC-Exchange and-Core are covered by the EOSC
support activities.

![eosc-platform-high-level](arch/platform-high-level-arch.png){: style="width:75%"}

*Diagram 3. EOSC Platform high-level architecture*

To better plan both operations and functionalities available to the resource
providers, there was a need to identify provider types that might be
potentially interested in joining EOSC. This allows for a better
understanding of emerging needs and the diversity in terms of policies, resources,
needed procedures and supporting tools.


![eosc-providers](arch/providers-scope-eosc.png){: style="width:70%"}

*Diagram 4. Providers recognised in scope of EOSC*

For the provider, when planning a new service or research product and the
delivery thereof, it is crucial to understand the targeted consumer base
well. To meet this requirement an analysis was performed for
EOSC Consumers. The result is shown in diagram 5.

![eosc-stakeholeders](arch/user-and-consumer-types.svg){: style="width:80%"}

*Diagram 5. Consumers recognised in scope of EOSC along with*
characteristics relevant in EOSC



## Core integration in EOSC

For the providers of horizontal resources (such as computing or storage)
EOSC Future is preparing its so called "Interoperability Framework" which
will define ways to connect new capacities to existing horizontal
services. While the general guidelines are under development, EOSC Synergy
implemented the required integrations for e-Infrastructure services
involved in the project. This integrations will be the subject of the next
chapters of this document.

When connecting your resources to an already existing horizontal service,
there is still a need to follow its integrations with the EOSC-Core layer.
A horizontal service, like all other EOSC services (such as thematic
services), may expose its user-support features to the EOSC consumer. For
that, a unified, user-centric environment that publishes data related to
service delivery (EOSC-Core) has been created.

It consists of 6 Core services, that focus on different aspects of making
services available to its users in a professional manner:

- Service discoverability supported by the EOSC Resource Catalogue and the
    EOSC Marketplace
- Service usability supported by the EOSC Order Management
- Service Monitoring
- Service Accounting
- User Support facilitated in the EOSC Helpdesk
- Service accessibility supported by the EOSC AAI Proxy

Depending on the infrastructure maturity of service delivery, there are
multiple options. Either the EOSC instances of Core services may be used,
or own Core instances may be deployed, to connect custom Core capabilities
to the central instances, so the EOSC Consumer is offered with more
advanced experience when using services.

![eosc-stakeholeders](arch/integration-possibilities.png){: style="width:90%"}

*Diagram 6. Overview of integration possibilities for a Resource Provider*
in EOSC

We will now focus on different given EOSC-Core service, and explain the
goals of the integration and the methods used. The descriptions are taken
from the EOSC-Core Interoperability Guidelines delivered by EOSC Future.

## EOSC Resource Catalogue

The EOSC Resource Catalogue is the heart of the EOSC Future ecosystem. It
provides data and functionality to register, maintain, administer
and share resources onboarded by various providers. Moreover, it is the
point of reference for all EOSC Platform components that provide added
value to the resource catalogue and help in making all contributed data and services
searchable and accessible using various tools, both for researchers and
end users.

The EOSC Resource Catalogue is the result of merging the EOSC Service
Catalogue (including data source services) and the EOSC Research Product
Catalogue, together with the related list of EOSC providers. These are
populated independently, as the resource onboarding process they support
differs due to the different nature of the resources they manage, but
their resources are interrelated. The services can acquire metadata
records from similar catalogues located within various locations, such as
Research-Infrastructures, clusters, within other organisations, or
directly from the data sources, in the case of research products. As a
result, the EOSC Resource catalogue contains metadata about services, data
sources, and research products, together with semantic relationships
between them, highlighting the nature of the connection between services
and products (e.g. hosted By, generated By, etc.)

![eosc-resource-catalogue](arch/res-cat-arch.png){: style="width:100%"}

*Diagram 7. EOSC Resource Catalogue high-level architecture*

For more details about this service and ways to integrate with it,
please refer to [Resource Catalogue Interoperability
Guidelines](https://wiki.eoscfuture.eu/display/PUBLIC/Resource+Catalogue+Architecture+and+Interoperability+Guidelines).

The Resource Catalogue is available at <https://providers.eosc-portal.eu/home>.

## Order Management

One of the formative aspects of the EOSC Platform was to facilitate the
collaboration between EOSC end-users and EOSC resource providers to
stimulate the uptake of EOSC Resources. By providing different levels of
support for various EOSC Resource access methods, the EOSC Marketplace
addresses the inherent diversity of the EOSC ecosystem. Among other
functionalities supporting the resource accessibility, Marketplace
provides end-user features to order resources, monitor user requests and
communicate with resource providers. For the providers in turn, the
Marketplace
offers various interoperability patterns to integrate order
management processes in alignment with a vision of a federated system of
systems. The implemented interoperability patterns are aimed at
individual providers and provider communities adopting the guidelines to
integrate their own resource provisioning mechanisms. The EOSC Order
Management process engages also the EOSC Platform Operations Team who
play a key role in the EOSC Platform CRM (Customer Relationship
Management), end-users support and guidance through the composability of
resources in the EOSC ecosystem.

The EOSC Marketplace facilitates the ordering process, being a connection
point between resources and order management systems (OMSes) for
resource providers. It is also an entry point for the users looking to
advance their research projects by using EOSC Resources. They can follow
the entire path from resource discovery to order fulfilment in a single
portal, meaning a coherent experience from their side.

On the other hand, providers and communities are met with several
flexible options to integrate their ordering process. Firstly, they can
specify offerings for their resources. Providers can configure their
offerings using an interoperable "Offering API" or ergonomic UI. Both are
flexible enough to support various offering use cases. Secondly, they
can handle orders placed in the system in several interoperable ways,
either utilising the existing SOMBO system (Service Order Management
Back Office), or using the Marketplace Ordering API. The latter allows
the integration of existing Order Management Systems while providing
an out-of-the-box support for Jira-based solutions as a reference
implementation of the integration.

![eosc-management-patterns](arch/order-management-patterns.png){: style="width:80%"}

*Diagram 8. EOSC Order Management interoperability patterns*

For more details about this service and ways to integrate with it,
please refer to the [Order Management Interoperability
Guidelines](https://wiki.eoscfuture.eu/display/PUBLIC/Order+Management+Architecture+and+Interoperability+Guidelines).

The EOSC Marketplace service is available at <https://marketplace.eosc-portal.eu>.

## Service Monitoring

Monitoring is the key service needed to gain insights into an
infrastructure. It needs to be continuous and on-demand to quickly
detect, correlate, and analyse data for a fast reaction to anomalous
behaviour. The challenge of this type of monitoring is how to quickly
identify and correlate problems before they affect end-users and
ultimately the productivity of the organisation. Management teams can
monitor the availability and reliability of the services from a high
level view down to individual system metrics and monitor the conformance
of multiple SLAs. The key functional requirements are:

- Monitoring of services,
- Reporting availability and reliability,
- Visualisation of the services status,
- Provide dashboard interfaces,
- Sending real-time alerts.

The dashboard design should enable easy access and visualisation of data
for end-users. APIs should also be supported so as to allow third
parties to gather monitoring data from the system through them.

The key requirements of a monitoring system are:

- Support for multiple entry points (different types of systems can
    work together)
- Interoperable
- High availability of the different components of the system
- Loosely coupled: support API in the full stack so that components are
    independent in their development cycles
- Support for multiple tenants, configurations, metrics and profiles to
    add flexibility and ease of customisation.

For EOSC there are two monitoring services already in place: EOSC-Core
and the EOSC Exchange Monitoring Service. These two services are
responsible to monitor the Core services (EOSC-Core Monitoring) and the
services onboarded to the Marketplace (EOSC-Exchange Monitoring).

For more details about this service and ways to integrate with it,
please refer to [Monitoring Interoperability
Guidelines](https://wiki.eoscfuture.eu/display/PUBLIC/Monitoring+Architecture+and+Interoperability+Guidelines).

The monitoring service is available at <https://argo.eosc-portal.eu>.

## Service Accounting

General Interoperability Guidelines for service accounting are yet
before publication but when ready, will be available (here)
[https://wiki.eoscfuture.eu/display/PUBLIC/Services+Accounting+Architecture+and+Interoperability+Guidelines].

However, as the 'horizontal integrations' in EOSC Synergy were conducted
with services offering service accounting, the specific approach towards
accounting in the scope of this project is presented later in this
document.

## EOSC Helpdesk

The EOSC Helpdesk is the entry point and ticketing system/request
tracker for issues concerning the available EOSC services. It implements
incident and service request management and provides efficient
communication channels between customers, users and providers of the IT
resources and services. The EOSC Helpdesk provides multiple
capabilities, which were established during requirement analysis, such
as self-service, reporting and notifications; it guarantees the
integrity of the IT infrastructure and quality of the delivered
services.

![eosc-stakeholeders](arch/helpdesk-arch.png){: style="width:80%"}

*Diagram 9. EOSC Helpdesk high-level architecture*

For more integration-oriented information about EOSC Helpdesk please
refer to this
[link](https://wiki.eoscfuture.eu/display/PUBLIC/Helpdesk+Architecture+and+Interoperability+Guidelines).

The EOSC Helpdesk is available at <https://eosc-helpdesk.eosc-portal.eu>.

## EOSC AAI Proxy

The integration with the Authentication and Authorisation Infrastructure
(AAI) is the fundamental step, because this is the mechanism that
delivers the identities (unique identifiers and group memberships) to
the infrastructure. This is essential in order for the users to access
the services in the first place. Furthermore, AAI is the logical "point"
where users may be informed about the policies (like privacy notices,
AUPs) and where access rights to services may be enforced.

The EOSC-Core Infrastructure Proxy is connecting the EOSC Core, EOSC
Exchange and EOSC Support Services to the EOSC Federated AAI.

At the moment, documentation and integration guidelines publicly
provided for joining AAI Infrastructure Proxy, do not recognise EOSC
Exchange Service Providers. The new version of guidelines is just being
finalised and are [available 
here](https://wiki.eoscfuture.eu/display/PUBLIC/EOSC+Core+Infrastructure+Proxy+-+Policy+for+connecting+services).


<!--


## AAI

The integration with the Authentication and Authorisation
Infrastructure (AAI) is the fundamental step, because this is the
mechanism that delivers the identities (unique identifiers and group
memberships) to the infrastructure. This is essential in order for the
users to access the services in the first place. Furthermore, AAI is
the logical "point" where users may be informed about the policies
(like privacy notices, AUPs) and where access rights to services may
be enforced.

In collaboration with EOSC-Hub and EGI we have recorded a procedure
\[AAI-Integration-Procedure\]. There, services are first integrated
with a demonstration instance of the EGI Check-in service for initial
testing. Successfully tested services will then be moved to the
production instance of EGI Check-in. This process is documented and
supported in various procedures of EGI \[EGI-Proc-09\].

This integration will allow users to access all services, using their
home-Identity, their community identities supported by EGI (six at the
time of writing) or one of six social identity providers. In addition,
Virtual Organisations (VOs) have been created \[EGI-Proc-14\] to
support the thematic services and facilitate their resource allocation
requests. For conducting trainings we envisage collaboration with the
EGI training VO.

##  Monitoring

Every production service requires a monitoring system, so the desired
efficiency and accountability can be verified at any time. After
defining what should be monitored, it is easy to create a test and
display its results. For EOSC-Synergy, Nagios core service will be
used. One of the key aspects of Nagios is to get the information about
a particular test.

With Nagios we can monitor various kinds of hosts, services and
actions. From the simplest check on a web service (ping) to a more
complex test on the service itself, such as testing outputs giving
some input on APIs, websites using selenium or even testing if the
login runs smoothly. This is all examples, since plugins can be
written to all types of tests to check even the small functionality,
using different programming languages.

In addition, contacts and alerts will be used to notify about problems
detected by the Nagios service. This allows us to efficiently react to
problems in a timely manner.

##  Accounting

The EOSC Accounting service collects, stores, aggregates, and displays
usage information of HTC compute, storage space, cloud VM and data set
resources. Resource Centres that are providing compute or storage to
the EOSC infrastructure have to implement a collector (a stand-alone
script or program, or a built-in function of their resource system),
that gathers accounting metrics formatted into a standardised record
format. These metrics are then transferred via a messaging service to
the Accounting Repository, which stores and processes the data to
produce aggregations that are then sent to the Accounting Portal for
display.

The Accounting Portal retrieves topology information on how resource
centres relate to national infrastructures and regions from the
configuration management database (CMDB) and community affiliation
from the AAI service to properly organise the accounting data.
Information related to groups or VOs should also contain information
about scientific disciplines to allow the portal to properly classify
the resource usage. The Accounting Portal already is integrated with
several other tools, such as GOCDB and REBUS for topology and
geographical data, Check-In for the AAI, the Operations Portal for VO
and scientific discipline information. It also uses X.509 certificates
to map users to institutions and these to countries.

##  Information Provider

The information system collects data from the resource providers in a
research infrastructure and makes it available for workload
orchestration. In the EGI e-Infrastructure this is a fundamental
service both for the HTC or Grid technology and for the EGI FedCloud.
There are different solutions for each type of service within those
technologies, e.g. both ARC and HTCondorCE have ad hoc provider
implementations for gathering the information. For Federated Cloud, we
use a unique implementation, coined as cloud-info-provider, to fetch
data from the supported Cloud Management Frameworks (CMFs), notably
OpenStack and OpenNebula. The cloud-info-provider component leverages
the APIs exposed by those CMFs to get key information about the Cloud
resource provider, such as the projects and images that any given VO
is allowed to use.

Consequently, the data collected by the information providers is
essential for the operation of the different workload management
services available through EOSC, such as the INDIGO PaaS Orchestrator
or DIRAC4EGI.

##  EOSC-Exchange / Marketplace

-   320+ Service, out of which the thematic service chose the most useful ones.
-   Interoperability Framework
-   Federation
-->
