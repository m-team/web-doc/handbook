# handbook

- This Handbook is licenced under the Creative Commons CC-BY-SA-3.0 licence.

- Contributions are welcome via pull-requests to https://github.com/EOSC-synergy/handbook/

- The Handbook is published to https://handbook.eosc-synergy.eu
