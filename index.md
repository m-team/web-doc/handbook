[![eosc-synergy-logo](https://www.eosc-synergy.eu/wp-content/uploads/logo-color-texto.png)](https://eosc-synergy.eu)

# EOSC-SYNERGY -- Handbook

This handbook is targeted at multiple different audiences. Different
sections are targeted at different groups. We suggest a prioritised list
to read for the different target audiences.

- **Management of Computer Centres**:
    1. [Introduction](introduction.md)
    1. [Architecture of EOSC](eosc-architecture.md)
    1. [Resource Integration](resource-integration.md)

- **Users of the cloud**:
    1. [Introduction](introduction.md)
    1. [Thematic Services](thematic-services.md)
    1. [EOSC Synergy Services and tools](synergy-tools.md)

- **System Administrators**:
    1. [Resource Integration](resource-integration.md)
    1. [Thematic Services](thematic-services.md)
    1. [Architecture of EOSC](eosc-architecture.md)
